# ip-whitelist



## Getting started

Upload a file to your public_html.

Include it in a file where you want to check if IP is white-listed.
```
<?php include("ipWhitelist.php"); ?>
```

## Configuration

Parameters
```
IP_WhiteList($whitelist, $errorRedirectFile, $logFailedAttemptFlag)
```

```
$whitelist :: list with whitelisted IPs (if you want to white-list all IPs, leave it empty)
$errorRedirectFile :: where to redirect a client when IP criteria is not met
$logFailedAttemptFlag :: true | false (if you want to log failed attempts)  [default = true]
```

Example
```
$ip_wl = ["8.8.4.4"];
$ipwl = new IP_WhiteList($ip_wl, "./blocked.php");
$ipwl->runWhiteListProtection();
```

## Wordpress

If you want to restrict IP access to WP-admin login, you have to edit file *wp-login.php*.

Add an include code at the top of the file.

```
<?php include("ipWhitelist.php"); ?>

<?php
/**
 * WordPress User Page
 *
 * Handles authentication, registering, resetting passwords, forgot password,
 * and other user handling.
 *
 * @package WordPress
 */
 ```

