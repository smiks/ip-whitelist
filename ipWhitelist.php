<?php
class IP_WhiteList {
    private function getClientIP()
    {
    	if (isset($_SERVER["HTTP_CLIENT_IP"]))
    	{
    		return $_SERVER["HTTP_CLIENT_IP"];
    	}
    	elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
    	{
    		return $_SERVER["HTTP_X_FORWARDED_FOR"];
    	}
    	elseif (isset($_SERVER["HTTP_X_FORWARDED"]))
    	{
    		return $_SERVER["HTTP_X_FORWARDED"];
    	}
    	elseif (isset($_SERVER["HTTP_FORWARDED_FOR"]))
    	{
    		return $_SERVER["HTTP_FORWARDED_FOR"];
    	}
    	elseif (isset($_SERVER["HTTP_FORWARDED"]))
    	{
    		return $_SERVER["HTTP_FORWARDED"];
    	}
    	else
    	{
    		return $_SERVER["REMOTE_ADDR"];
    	}
    }
    
    private function logFailedAttempt($ip) {
        $file = 'IP_Whitelist_log';
        $dtm = date('Y-m-d H:i:s');

        $current = file_get_contents($file);
        $current .= "Blocked access from {$ip} @ {$dtm} \n";
        file_put_contents($file, $current);
    }
    
   	public function __construct($whitelist, $errorRedirectFile, $logFailedAttemptFlag = true){
   	    $this->whiteList = $whitelist;
   	    $this->errorRedirectFile = $errorRedirectFile;
   	    $this->logFailedAttemptFlag = $logFailedAttemptFlag;
	}
	
	public function runWhiteListProtection() {
	    $ip = $this->getClientIP();
	    
        if(sizeOf($this->whiteList) > 0 && !in_array($ip, $this->whiteList)){
            if($this->logFailedAttemptFlag){
                try {
                    $this->logFailedAttempt($ip);    
                } catch(Exception $e){
                   
                }
                
            }
            header("Location: " . $this->errorRedirectFile);
        }	    
	}
}

$ip_wl = ["93.103.127.189"];
$ipwl = new IP_WhiteList($ip_wl, "./blocked.php");
$ipwl->runWhiteListProtection();

